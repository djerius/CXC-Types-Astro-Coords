# NAME

CXC::Types::Astro::Coords - type definitions for Coordinates

# VERSION

version 0.13

# SYNOPSIS

    use CXC::Types::Astro::Coords -types;

# DESCRIPTION

**CXC::Types::Astro::Coords** provides [Type::Tiny](https://metacpan.org/pod/Type%3A%3ATiny) compatible types for coordinate
conventions used in Astronomy.

# TYPES

## Sexagesimal\[\`a\]

    $type = Sexagesimal[ @flags ];

    $type = Sexagesimal; #  same as Sexagesimal[ qw( -any -optws -optsep -optunits -trim )]

Return a type tuned to recognize specific forms of sexagesimal
coordinate notation.  See the [CXC::Types::Astro::Coords::Util](https://metacpan.org/pod/CXC%3A%3ATypes%3A%3AAstro%3A%3ACoords%3A%3AUtil)
**mkSexagesimal** subroutine for more information on the available
flags.

## SexagesimalArray

An array of three values (_degrees_, _minutes_, _seconds_), where the values have ranges and types

    [unbounded]  <integer>
    [0, 59]      <integer>
    [0, 60)      <float>

Coercible from a string matching ["Sexagesimal"](#sexagesimal).

## SexagesimalDegrees

A real number.

Coercible from either ["SexagesimalArray"](#sexagesimalarray) or ["Sexagesimal"](#sexagesimal)

## Degrees

A real number in the range \[0,360). Numbers are coerced by modulo 360.

## LongitudeArray

An array of three values (_degrees_, _minutes_, _seconds_), where the values have ranges and types

    [0,359]  <integer>
    [0, 59]  <integer>
    [0, 60)  <float>

Coercible from a string matching ["LongitudeSexagesimal"](#longitudesexagesimal).

## LongitudeDegrees

A real number in the range \[0,360)

Coercible from either ["LongitudeArray"](#longitudearray) or ["LongitudeSexagesimal"](#longitudesexagesimal)

## LongitudeSexagesimal

A string with three components (_degrees_, _minutes_, _seconds_),
optionally separated by one of

- white space;
- the `:` character;
- component specific suffices of `d`, `m`, or `s`.

The components have ranges of:

    [0, 359]  <integer>
    [0,  59]  <integer>
    [0,  60)  <integer/float>

## LatitudeArray

An array of three values (_degrees_, _minutes_, _seconds_), where
the values have ranges and types of

    [-90, 90]  <integer>
    [  0, 59]  <integer>
    [  0, 60)  <integer/float>

and

    abs($A[0]) + ( $A[1] + $A[2] / 60 ) / 60 <= 90

Coercible from a string matching ["LatitudeSexagesimal"](#latitudesexagesimal).

## LatitudeDegrees

A real number in the range \[-90,+90\]

Coercible from either ["LatitudeArray"](#latitudearray) or ["LatitudeSexagesimal"](#latitudesexagesimal)

## LatitudeSexagesimal

A string with three components (_degrees_, _minutes_, _seconds_) optionally separated by one of

- white space
- the `:` character,
- component specific suffices of `d`, `m`, or `s`

The components have ranges and types of

    [-90, 90]  <integer>
    [  0, 59]  <integer>
    [  0, 60)  <integer/float>

and

    abs($A[0]) + ( $A[1] + $A[2] / 60 ) / 60 <= 90

## RightAscensionArray

An array of three values (_hours_, _minutes_, _seconds_), where the values have ranges and types of

    [0, 23]  <integer>
    [0, 59]  <integer>
    [0, 60)  <integer/float>

Coercible from a string matching ["RightAscensionSexagesimal"](#rightascensionsexagesimal).

## RightAscensionDegrees

A real number in the range \[0,360\]

Coercible from either ["RightAscensionArray"](#rightascensionarray) or ["RightAscensionSexagesimal"](#rightascensionsexagesimal)

## RightAscensionSexagesimal

A string with three components (_hours_, _minutes_, _seconds_) optionally separated by one of

- white space
- the `:` character,
- component specific suffices of `h`, `m`, or `s`

The components have ranges and types of:

    [0, 23]  <integer>
    [0, 59]  <integer>
    [0, 60)  <integer/float>

## DeclinationArray

An array of three values (_degrees_, _minutes_, _seconds_), where the values have ranges and types of

    [-90, 90]  <integer>
    [  0, 59]  <integer>
    [  0, 60)  <integer/float>

and

    abs($A[0]) + ( $A[1] + $A[2] / 60 ) / 60 <= 90

Coercible from a string matching ["DeclinationSexagesimal"](#declinationsexagesimal).

## DeclinationDegrees

A real number in the range \[-90,+90\]

Coercible from either ["DeclinationArray"](#declinationarray) or ["DeclinationSexagesimal"](#declinationsexagesimal)

## DeclinationSexagesimal

A string with three components (_degrees_, _minutes_, _seconds_), optionally separated by one of

- white space;
- the `:` character;
- component specific suffices of `d`, `m`, or `s`.

The components have ranges and types of

    [-90, 90]  <integer>
    [  0, 59]  <integer>
    [  0, 60)  <integer/float>

and

    abs($A[0]) + ( $A[1] + $A[2] / 60 ) / 60 <= 90

## SexagesimalHMS

A string with three components (_hours_, _minutes_, _seconds_).
Each component consists of a number and a suffix of `h`,`m`, `s`.
Components may be separated by white space.

The components have ranges and types of:

    [0, 23]  <integer>
    [0, 59]  <integer>
    [0, 60)  <integer/float>

## SexagesimalDMS

A string with three components  (_degrees_, _minutes_, _seconds_).
Each component consists of a number, optional white space, and a
suffix of `d`,`m`, `s`.  Components may be separated by white space.

The components have ranges and types of:

    [0, 359]  <integer>
    [0,  59]  <integer>
    [0,  60)  <integer/float>

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-types-astro-coords@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Types-Astro-Coords](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Types-Astro-Coords)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-types-astro-coords

and may be cloned from

    https://gitlab.com/djerius/cxc-types-astro-coords.git

# AUTHORS

- Terry Gaetz <tgaetz@cfa.harvard.edu>
- Diab Jerius <djerius@cfa.harvard.edu>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2018 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
